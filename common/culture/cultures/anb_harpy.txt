nansalyra = {
	color = { 143  176  234 }
	
	ethos = ethos_courtly
	heritage = heritage_harpy
	language = language_zanite
	martial_custom = martial_custom_female_only
	traditions = {
		tradition_mountain_homes
		tradition_ep2_avid_falconers
		tradition_longbow_competitions
		tradition_poetry
		tradition_female_only_inheritance
	}
	
	name_list = name_list_firanyan
	
	coa_gfx = { arabic_group_coa_gfx iranian_group_coa_gfx }
	building_gfx = { mena_building_gfx }
	clothing_gfx = { dde_abbasid_clothing_gfx mena_clothing_gfx iranian_clothing_gfx }
	unit_gfx = { iranian_unit_gfx }
	
	ethnicities = {
		25 = arab
		75 = mediterranean_byzantine
	}
}

heunthulyra = {
	color =  { 200 169 168 }
	
	ethos = ethos_courtly
	heritage = heritage_harpy
	language = language_firanyan
	martial_custom = martial_custom_female_only
	traditions = {
		tradition_mountain_homes
		tradition_storytellers
		tradition_tribe_unity
		tradition_polygamous
		tradition_female_only_inheritance
	}
	
	name_list = name_list_firanyan
	
	coa_gfx = { arabic_group_coa_gfx iranian_group_coa_gfx }
	building_gfx = { mena_building_gfx }
	clothing_gfx = { dde_abbasid_clothing_gfx mena_clothing_gfx iranian_clothing_gfx }
	unit_gfx = { iranian_unit_gfx }
	
	ethnicities = {
		25 = arab
		75 = mediterranean_byzantine
	}
}

firanyalyra = {
	color = { 50 100 30 }
	
	ethos = ethos_spiritual
	heritage = heritage_harpy
	language = language_firanyan
	martial_custom = martial_custom_female_only
	traditions = {
		tradition_mountain_homes
		tradition_storytellers
		tradition_adaptive_skirmishing
		tradition_female_only_inheritance
	}
	
	name_list = name_list_firanyan
	
	coa_gfx = { arabic_group_coa_gfx iranian_group_coa_gfx }
	building_gfx = { mena_building_gfx }
	clothing_gfx = { dde_abbasid_clothing_gfx mena_clothing_gfx iranian_clothing_gfx }
	unit_gfx = { iranian_unit_gfx }
	
	ethnicities = {
		25 = arab
		75 = mediterranean_byzantine
	}
}

siadannira = {
	color = { 173 228 219 }
	
	ethos = ethos_bellicose
	heritage = heritage_harpy
	language = language_siadunan
	martial_custom = martial_custom_female_only
	traditions = {
		tradition_mountain_homes
		tradition_ruling_caste
		tradition_adaptive_skirmishing
		tradition_female_only_inheritance
	}
	
	name_list = name_list_siadunan
	
	coa_gfx = { arabic_group_coa_gfx iranian_group_coa_gfx }
	building_gfx = { mena_building_gfx }
	clothing_gfx = { dde_abbasid_clothing_gfx mena_clothing_gfx iranian_clothing_gfx }
	unit_gfx = { iranian_unit_gfx }
	
	ethnicities = {
		25 = arab
		75 = mediterranean_byzantine
	}
}