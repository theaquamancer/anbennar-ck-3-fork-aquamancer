tradition_precursor_legions = {
	category = combat

	layers = {
		0 = martial
		1 = mediterranean
		4 = laurel.dds
	}

	is_shown = {
		has_cultural_pillar = heritage_elven
	}
	can_pick = {
		has_cultural_pillar = heritage_elven
	}
	
	character_modifier = {
		levy_size = -0.35
		men_at_arms_recruitment_cost = -0.25
		men_at_arms_maintenance = -0.35
		levy_reinforcement_rate = -0.35
		men_at_arms_cap = 1
		pikemen_max_size_add = 2
		archers_max_size_add = 2
		hard_casualty_modifier = -0.15
	}
	
	cost = {
		prestige = {
			add = {
				value = tradition_base_cost
				desc = BASE
				format = "BASE_VALUE_FORMAT"
			}
			if = {
				limit = {
					NOR = {
						culture_pillar:ethos_bellicose = { is_in_list = traits }
						culture_pillar:ethos_egalitarian = { is_in_list = traits }
						culture_pillar:ethos_courtly = { is_in_list = traits }
					}
				}
				add = {
					value = tradition_incompatible_ethos_penalty
					desc = not_bellicose_egalitarian_or_courtly_desc
				}
			}
			
			multiply = tradition_replacement_cost_if_relevant
		}
	}
	
	ai_will_do = {
		value = 100
	}
}

tradition_havoral_goatherding = {
	category = realm
	
	layers = {
		0 = intrigue
		1 = western
		4 = mountain.dds
	}
	
	can_pick = {
		OR = {
			this = culture:havoric
			any_parent_culture_or_above = {
				this = culture:havoric
			}
		}
	}
	
	parameters = {
		rough_terrain_expert_trait_more_common = yes
		can_recruit_forest_specialist = yes
		hills_hunt_success_chance = yes
	}
	province_modifier = {
		mountains_development_growth_factor = 0.15
		hills_development_growth_factor = 0.2
		mountains_construction_gold_cost = -0.1
		hills_construction_gold_cost = -0.1
	}
	character_modifier = {
		mountains_travel_danger = mountains_low_danger_reduction
		hills_travel_danger = hills_low_danger_reduction
	}
	
	cost = {
		prestige = {
			add = {
				value = tradition_base_cost
				desc = BASE
				format = "BASE_VALUE_FORMAT"
			}
			if = {
				limit = {
					NOR = {
						culture_pillar:ethos_bellicose = { is_in_list = traits }
						culture_pillar:ethos_stoic = { is_in_list = traits }
						culture_pillar:ethos_communal = { is_in_list = traits }
					}
				}
				add = {
					value = tradition_incompatible_ethos_penalty
					desc = not_bellicose_stoic_or_communal_desc
				}
			}
			multiply = tradition_replacement_cost_if_relevant
		}
	}
	
	ai_will_do = {
		value = 100
	}
}
