﻿###DECISIONS LIST###

#form_castellyr_decision

# Castellyr was formed in 1339 by a peaceful union of the two kingdoms of Cast and Anor
form_castellyr_decision = {
	picture = {
		reference = "gfx/interface/illustrations/decisions/decision_realm.dds"
	}
	decision_group_type = major
	ai_check_interval = 60
	desc = form_castellyr_decision_desc

	is_shown = {
		OR = {
			has_culture = culture:castanorian
			has_culture = culture:black_castanorian
		}
		#Both Cast and Anor have de jure land
		title:k_cast = { any_in_de_jure_hierarchy = { tier = tier_county } }
		title:k_anor = { any_in_de_jure_hierarchy = { tier = tier_county } }
		NOT = {
			is_target_in_global_variable_list = {
				name = unavailable_unique_decisions
				target = flag:form_castellyr_decision
			}
		}
		highest_held_title_tier < tier_empire
	}

	is_valid = {
		# need to be castanorian
		OR = {
			culture = culture:castanorian
			culture = culture:black_castanorian
		}
		# control all of inner castanor
		completely_controls_region = custom_inner_castanor
		# need both Cast and Anor
		has_title = title:k_cast
		has_title = title:k_anor
		# if the kigndom of Castonath exist you must have it
		trigger_if = {
			limit = { exists = title:k_castonath.holder }
			has_title = title:k_castonath
		}
		OR = {
			has_primary_title = title:k_cast
			has_primary_title = title:k_anor
		}
	}

	effect = {
		save_scope_as = castellyr_former

		show_as_tooltip = { form_castellyr_decision_effects = yes } #Actually applied in anb_decision_major_events.0001 - prestige, laws, title changes

		#Events
		trigger_event = anb_decision_major_events.0001
		every_player = {
			limit = {
				NOT = { this = scope:castellyr_former }
				is_within_diplo_range = { CHARACTER = scope:castellyr_former }
			}
			trigger_event = anb_decision_major_events.0002
		}

		#Can only be done once
		add_to_global_variable_list = {
			name = unavailable_unique_decisions
			target = flag:form_castellyr_decision
		}
		set_global_variable = {
			name = form_castellyr_decision
			value = scope:castellyr_former
		}
	}

	ai_potential = {
		always = yes
	}

	ai_will_do = {
		base = 100
	}
}

claim_liege_title_invalid_race = {
	picture = {
		reference = "gfx/interface/illustrations/decisions/decision_realm.dds"
	}
	decision_group_type = major
	ai_check_interval = 60
	desc = claim_liege_title_invalid_race_desc

	is_shown = {
		is_independent_ruler = no
		liege = {
		 	any_held_title = {
				has_title_law_flag = racial_legitimacy_law
				character_is_legitimate_race_for_title_laws = {
					CHARACTER = root
					TITLE = this
				}
			}
		}
	}

	is_valid = {
		is_powerful_vassal = yes
		custom_description = {
			text = liege_is_illegitimate_race_for_title_trigger	
			liege = {
				any_held_title = {
					character_is_illegitimate_race_for_title = {
						TITLE = this
						CHARACTER = root.liege
					}
					any_in_de_jure_hierarchy = {
						holder = root
					}
					character_is_legitimate_race_for_title_laws = {
						CHARACTER = root
						TITLE = this
					}
					NOT = { any_claimant = { this = root } }
				}
			}
		}
	}

	effect = {
		liege = {
			every_held_title = {
				limit = {
				 	character_is_illegitimate_race_for_title = {
				 		TITLE = this
				 		CHARACTER = root.liege
					}
					any_in_de_jure_hierarchy = {
						holder = root
					}
					character_is_legitimate_race_for_title_laws = {
						CHARACTER = root
						TITLE = this
					}
					NOT = { any_claimant = { this = root } }
				}
				add_to_temporary_list = racial_titles_to_claim
			}
		}
		every_in_list = {
			list = racial_titles_to_claim
			root = { add_unpressed_claim = prev }
		}
	}

	ai_potential = {
		always = yes
	}

	ai_will_do = {
		base = 100
	}
}

revive_bladestewards_decision = {
	picture = {
		reference = "gfx/interface/illustrations/decisions/decision_major_religion.dds"
	}
	decision_group_type = major
	ai_check_interval = 60

	is_shown = {
		# Standard filter checks.
		NOT = { bladestewards_exist_trigger = yes }
		is_landed = yes
		exists = dynasty
		OR = {
			has_government = tribal_government
			has_government = feudal_government
			has_government = clan_government
		}
		has_title = title:k_blademarches
		# Bladestewards can exist
		bladestewards_can_exist_trigger = yes
	}

	is_valid = {
		has_trait = bladeshunned
		# Should be decently famous in all fashions.
		prestige_level >= medium_prestige_level
		# You hold the Kingdom of Blademarches
		has_title = title:k_blademarches
		has_title = title:c_stewards_hold
	}

	is_valid_showing_failures_only = {
		is_available_adult = yes
	}

	effect = {
		save_scope_as = founder
		# Establish the Bladestewards - Needs to be changed to ONLY make the Bladestewards Holy Order
		#establish_the_bladestewards_tooltip_effect = yes
		
		remove_trait = bladeshunned
		
		title:c_stewards_hold = {
			add_county_modifier = bladestewards_present
		}
		
		have_bladesteward_wield_calindal_effect = yes
	}

	cost = {
		gold = 500
		prestige = 1000
	}

	ai_potential = {
		always = yes
	}

	ai_will_do = {
		base = 100
	}
}

draw_calindal = {
	picture = {
		reference = "gfx/interface/illustrations/decisions/decision_major_religion.dds"
	}
	decision_group_type = major
	ai_check_interval = 365
	is_shown = {
		OR = {
			# Don't have a claim and don't own the title
			AND = {
				NOT = { has_title = title:k_blademarches }
				NOT = { has_claim_on = title:k_blademarches }
			}
			# OR haven't tried to wield it yet
			AND = {
				NOT = { has_character_modifier = blinded_by_calindal }
				NOT = { has_character_modifier = wielded_calindal }
				age > 14
			}
		}
		custom_description = {
			text = owns_calindal
			any_character_artifact = {
				has_variable = is_calindal
			}
		}
	}

	is_valid = {
		custom_description = {
			text = owns_calindal
			any_character_artifact = {
				has_variable = is_calindal
			}
		}
	}

	is_valid_showing_failures_only = {
		is_available_adult = yes
		NOT = { 
			has_character_modifier = blinded_by_calindal
		}
		NOT = {
			has_trait = blind
		}
	}

	effect = {
		show_as_tooltip = { blademarches_succession_determine_success = yes }
		trigger_event = blademarches_succession.0006
	}

	ai_potential = {
		always = yes
	}

	ai_will_do = {
		base = 100
	}
}


unite_the_vernman_kingdoms_decision = {
	picture = {
		reference = "gfx/interface/illustrations/decisions/decision_found_kingdom.dds"
	}
	decision_group_type = major
	ai_check_interval = 60
	desc = unite_the_vernman_kingdoms_decision_desc

	is_shown = {
		OR = {
			culture = culture:vernid
			culture = culture:vernman
		}
		game_start_date >= 1000.1.1
		OR = {
			calc_true_if = { #At least 2 of the titles has de jure land
				amount >= 2
				title:k_verne= { any_in_de_jure_hierarchy = { tier = tier_county } }
				title:k_menibor = { any_in_de_jure_hierarchy = { tier = tier_county } }
				title:k_armanhal = { any_in_de_jure_hierarchy = { tier = tier_county } }
			}
		}
		NOT = {
			is_target_in_global_variable_list = {
				name = unavailable_unique_decisions
				target = flag:unite_the_vernman_kingdoms_decision
			}
		}
		highest_held_title_tier < tier_empire
	}

	is_valid = {
		OR = {
			culture = culture:vernid
			culture = culture:vernman
		}
		trigger_if = {
			limit = { title:k_verne = { any_in_de_jure_hierarchy = { tier = tier_county } } } #Has de jure land
			completely_controls = title:k_verne
			has_title = title:k_verne
		}
		trigger_if = {
			limit = { title:k_menibor = { any_in_de_jure_hierarchy = { tier = tier_county } } } #Has de jure land
			completely_controls = title:k_menibor
			has_title = title:k_menibor
		}
		trigger_if = {
			limit = { title:k_armanhal = { any_in_de_jure_hierarchy = { tier = tier_county } } } #Has de jure land
			completely_controls = title:k_armanhal
			has_title = title:k_armanhal
		}
		custom_tooltip = {
   			text = unite_the_vernman_kingdoms_decision_primary
			OR = {
				has_primary_title = title:k_verne
				has_primary_title = title:k_menibor
				has_primary_title = title:k_armanhal
			}
		}
		OR = {
			has_realm_law = crown_authority_2
			has_realm_law = crown_authority_3
		}
	}

	effect = {
		save_scope_as = verne_uniter

		show_as_tooltip = { unite_the_vernman_kingdoms_decision_effects = yes } #Actually applied in anb_decision_major_events.0005 - prestige, laws, title changes

		#Events
		trigger_event = anb_decision_major_events.0005
		every_player = {
			limit = {
				NOT = { this = scope:verne_uniter }
				is_within_diplo_range = { CHARACTER = scope:verne_uniter }
			}
			trigger_event = anb_decision_major_events.0006
		}

		#Can only be done once
		add_to_global_variable_list = {
			name = unavailable_unique_decisions
			target = flag:unite_the_vernman_kingdoms_decision
		}
		set_global_variable = {
			name = unite_the_vernman_kingdoms_decision
			value = scope:verne_uniter
		}
		set_global_variable = {
			name = unite_the_vernman_kingdoms_decision_title
			value = scope:verne_uniter.primary_title
		}
	}

	ai_potential = {
		always = yes
	}

	ai_will_do = {
		base = 100
	}
}