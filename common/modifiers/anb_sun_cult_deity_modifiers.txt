﻿#Modifers for Sun Cults faiths (deity)

edurunigal_deity = {
	icon = county_modifier_development_positive
	
}

nimensar_deity = {
	icon = stewardship_positive
	
}

ninu_deity = {
	icon = treatment_positive
	
}

suhus_deity = {
	icon = stewardship_positive
	
}

zamagur_deity = {
	icon = martial_positive
	
}

dumegir_deity = {
	icon = diplomacy_positive
	
}

gelsibir_deity = {
	icon = food_positive
	
}

tambora_deity = {
	icon = stewardship_positive
	
}

kirasi_deity = {
	icon = drink_positive
	
}

gikud_deity = {
	icon = letter_positive
	
}

genna_deity = {
	icon = family_positive
	
}

pinnagar_deity = {
	icon = spoon_positive
	
}

sidim_deity = {
	icon = magic_positive
	
}

sarnagir_deity = {
	icon = prestige_positive
	
}

lahmas_deity = {
	icon = magic_positive
	
}

brasan_deity = {
	icon = magic_positive
	
}

amasuri_deity = {
	icon = family_positive
	
}

#Modifers for Sundancer Paragons (deity)

allania_deity = {
	icon = martial_positive
	
}

elizarian_deity = {
	icon = martial_positive
	
}

jaherion_deity = {
	icon = martial_positive
	
}

jaerelian_deity = {
	icon = martial_positive
	
}

aldarien_deity = {
	icon = martial_positive
	
}

erelessa_deity = {
	icon = martial_positive
	
}

velara_deity = {
	icon = martial_positive
	
}

lezlindel_deity = {
	icon = martial_positive
	
}

andrellion_deity = {
	icon = martial_positive
	
}

jaddarion_deity = {
	icon = martial_positive
	
}

taelarian_deity = {
	icon = martial_positive
	
}

daraztarion_deity = {
	icon = martial_positive
	
}