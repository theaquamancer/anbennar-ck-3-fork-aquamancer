﻿# Anbennar: commented vanilla stuff

# is_christian_faith = { } # Anbennar

# is_islamic_faith = { } # Anbennar

# is_jewish_faith = { } # Anbennar

# is_eastern_faith = { } # Anbennar

is_gnostic_faith = {
	group = "special"
	
	is_available_on_create = {
		has_doctrine = special_doctrine_is_gnostic_faith
	}

	special_doctrine_is_gnostic_faith = {
		parameters = {
			hostility_override_tenet_gnosticism = 0
			hostility_override_special_doctrine_is_gnostic_faith = 0
			granting_titles_gives_stress = yes
		}

		traits = {
			virtues = { temperate }
			sins = { gluttonous }
		}

		character_modifier = {
			learning = 2
			stewardship = -2
		}
	}
}

special_tolerance = {
	group = "main_group"
	
	is_available_on_create = {
		always = no # Goes away when creating a new Faith
	}
	
	special_doctrine_ecumenical_christian = {
		parameters = {
			hostility_override_special_doctrine_ecumenical_christian = 1
		}
	}
}

heresy_hostility = {
	group = "not_creatable"
	is_available_on_create = {
		has_doctrine = heresy_hostility_doctrine
	}
	heresy_hostility_doctrine = {
		parameters = {
			hostility_same_religion = 2
		}
	}
}

nudity_doctrine = {
	group = "special"
	is_available_on_create = {
		has_doctrine = special_doctrine_naked_priests
	}

	special_doctrine_naked_priests = {
		parameters = {
			naked_priests_active = yes
		}
	}
}

unreformed_faith = {
	group = "not_creatable"
	is_available_on_create = {
		# Goes away on reformation
		always = no
	}
	unreformed_faith_doctrine = {
		visible = no
		parameters = {
			# unreformed = yes also serves as the basis for the unreformed_syncretic tenet.
			unreformed = yes
			hostility_override_tenet_unreformed_syncretism = 2
			unreformed_syncretic_recipient_opinion_active = yes
			opinion_of_unreformed_syncretic_actor_opinion_active = 30
			conquest_cb_enabled = yes
			faith_can_raid = yes
		}

		character_modifier = {
			monthly_prestige_gain_mult = 0.2
			clan_government_vassal_opinion = -20
			feudal_government_vassal_opinion = -20
			republic_government_vassal_opinion = -20
		}
	}
	# west_african_unreformed_faith_doctrine = { }
}

divine_destiny = {
	group = "special"
	is_available_on_create = {
		has_doctrine = divine_destiny_doctrine
	}
	divine_destiny_doctrine = {
		piety_cost = {
			value = faith_doctrine_cost_low
		}
		parameters = {
			divine_destiny_holy_war_cost_reduction = yes
		}
	}
}

# adoptionist_school = { } # Anbennar

not_allowed_to_hof = {
	group = "special"

	is_available_on_create = {
		# Removed on reformation.
		always = no
	}
	
	special_doctrine_not_allowed_to_hof = {
		name = {
			first_valid = {
				# triggered_desc = { # Anbennar
					# trigger = {
						# religion_tag = judaism_religion
					# }
					# desc = prophecy_of_malachi_name
				# }
				desc = special_doctrine_not_allowed_to_hof_name
			}
		}

		desc = {
			first_valid = {
				# triggered_desc = { # Anbennar
					# trigger = {
						# religion_tag = judaism_religion
					# }
					# desc = prophecy_of_malachi_desc
				# }
				desc = special_doctrine_not_allowed_to_hof_desc
			}
		}
		parameters = {
			block_from_create_hof_parameter = yes
		}
	}
}

is_bulwari_faith = {
	group = "special"
	
	is_available_on_create = {
		has_doctrine = special_doctrine_is_sun_cult_faith
	}

	special_doctrine_is_sun_cult_faith = {
		parameters = {
			# Since doctrines cannot modify other faith's views of us, only our view of others, all Bulwari faiths are given this special doctrine which makes them friendlier to Faiths with the Sun Cult Syncretism tenet.
			hostility_override_tenet_sun_cult_syncretism = 1
			bulwari_syncretic_recipient_opinion_active = yes
			opinion_of_bulwari_syncretic_actor_opinion_active = 30
		}
	}
}

is_regent_court_faith = {
	group = "special"
	
	is_available_on_create = {
		has_doctrine = special_doctrine_is_regent_court_faith
	}

	special_doctrine_is_regent_court_faith = {
		parameters = {
			# Since doctrines cannot modify other faith's views of us, only our view of others, all Cannorian faiths are given this special doctrine which makes them friendlier to Faiths with the Regent Court Syncretism tenet.
			hostility_override_tenet_regent_court_syncretism = 1
			#Hostile to hidden infernalists, open infernalists are evil
			hostility_override_doctrine_hidden_infernalist = 2
			hostility_override_doctrine_open_infernalist = 3
			cannorian_syncretic_recipient_opinion_active = yes
			opinion_of_cannorian_syncretic_actor_opinion_active = 30
		}
	}
}

is_dwarven_faith = {
	group = "special"
	
	is_available_on_create = {
		has_doctrine = special_doctrine_is_dwarven_faith
	}

	special_doctrine_is_dwarven_faith = {
		parameters = {
			# Since doctrines cannot modify other faith's views of us, only our view of others, all Dwarven faiths are given this special doctrine which makes them friendlier to Faiths with the Dwarven Syncretism tenet.
			hostility_override_tenet_dwarven_syncretism = 1
			dwarven_syncretic_recipient_opinion_active = yes
			opinion_of_dwarven_syncretic_actor_opinion_active = 30
		}
	}
}

#Game rule only
full_tolerance = {
	group = "special"
	is_available_on_create = {
		has_doctrine = special_doctrine_full_tolerance
	}
	
	special_doctrine_full_tolerance = {
		parameters = {
			hostility_override_special_doctrine_full_tolerance = 0
		}
	}
}

