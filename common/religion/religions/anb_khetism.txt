﻿khetism_religion = {
	family = rf_divenori
	graphical_faith = muslim_gfx
	doctrine = kheteratan_hostility_doctrine 
	
	doctrine = doctrine_khetarchy

	#Main Group
	doctrine = doctrine_spiritual_head	
	doctrine = doctrine_gender_equal
	doctrine = doctrine_pluralism_pluralistic
	doctrine = doctrine_theocracy_temporal

	#Marriage
	doctrine = doctrine_concubines
	doctrine = doctrine_divorce_allowed
	doctrine = doctrine_bastardry_legitimization
	doctrine = doctrine_consanguinity_aunt_nephew_and_uncle_niece

	#Crimes
	doctrine = doctrine_homosexuality_shunned
	doctrine = doctrine_adultery_men_shunned
	doctrine = doctrine_adultery_women_shunned
	doctrine = doctrine_kinslaying_close_kin_crime
	doctrine = doctrine_deviancy_shunned
	doctrine = doctrine_witchcraft_crime

	#Clerical Functions
	doctrine = doctrine_clerical_function_taxation
	doctrine = doctrine_clerical_gender_either
	doctrine = doctrine_clerical_marriage_allowed
	doctrine = doctrine_clerical_succession_spiritual_appointment
	
	#Allow pilgrimages
	doctrine = doctrine_pilgrimage_encouraged
	
	#Funeral tradition
	doctrine = doctrine_funeral_mummification
	
	traits = {
		virtues = {
			content
			gregarious
			calm
		}
		sins = {
			ambitious
			wrathful
			lazy
		}
	}

	custom_faith_icons = {	#TODO
		khetarchy custom_faith_1 custom_faith_2 custom_faith_3 custom_faith_4 custom_faith_5 custom_faith_6 custom_faith_7 custom_faith_8 custom_faith_9 custom_faith_10 dualism_custom_1 zoroastrian_custom_1 zoroastrian_custom_2
	}

	holy_order_names = {	#TODO
		{ name = "holy_order_vyuha_of_the_temple_of_place" }
	}

	holy_order_maa = { armored_footmen }	#todo

	localization = {
		#HighGod - Elikhet
		HighGodName = khetism_high_god_name
		HighGodNamePossessive = khetism_high_god_name_possessive
		HighGodNameSheHe = CHARACTER_SHEHE_HE
		HighGodHerselfHimself = CHARACTER_HIMSELF
		HighGodHerHis = CHARACTER_HERHIS_HIS
		HighGodNameAlternate = khetism_high_god_name_alternate
		HighGodNameAlternatePossessive = khetism_high_god_name_alternate_possessive

		#Creator
		CreatorName = khetism_creator_god_name
		CreatorNamePossessive = khetism_creator_god_name_possessive
		CreatorSheHe = CHARACTER_SHEHE_HE
		CreatorHerHis = CHARACTER_HERHIS_HIS
		CreatorHerHim = CHARACTER_HERHIM_HIM

		#HealthGod
		HealthGodName = khetism_health_god_name
		HealthGodNamePossessive = khetism_health_god_name_possessive
		HealthGodSheHe = CHARACTER_SHEHE_SHE
		HealthGodHerHis = CHARACTER_HERHIS_HER
		HealthGodHerHim = CHARACTER_HERHIM_HER
		
		#FertilityGod
		FertilityGodName = khetism_fertility_god_name
		FertilityGodNamePossessive = khetism_fertility_god_name_possessive
		FertilityGodSheHe = CHARACTER_SHEHE_SHE
		FertilityGodHerHis = CHARACTER_HERHIS_HER
		FertilityGodHerHim = CHARACTER_HERHIM_HER

		#WealthGod
		WealthGodName = khetism_wealth_god_name
		WealthGodNamePossessive = khetism_wealth_god_name_possessive
		WealthGodSheHe = CHARACTER_SHEHE_HE
		WealthGodHerHis = CHARACTER_HERHIS_HIS
		WealthGodHerHim = CHARACTER_HERHIM_HIM

		#HouseholdGod
		HouseholdGodName = khetism_household_god_name
		HouseholdGodNamePossessive = khetism_household_god_name_possessive
		HouseholdGodSheHe = CHARACTER_SHEHE_SHE
		HouseholdGodHerHis = CHARACTER_HERHIS_HER
		HouseholdGodHerHim = CHARACTER_HERHIM_HER

		#FateGod
		FateGodName = khetism_fate_god_name
		FateGodNamePossessive = khetism_fate_god_name_possessive
		FateGodSheHe = CHARACTER_SHEHE_IT
		FateGodHerHis = CHARACTER_HERHIS_ITS
		FateGodHerHim = CHARACTER_HERHIM_IT

		#KnowledgeGod
		KnowledgeGodName = khetism_knowledge_god_name
		KnowledgeGodNamePossessive = khetism_knowledge_god_name_possessive
		KnowledgeGodSheHe = CHARACTER_SHEHE_SHE
		KnowledgeGodHerHis = CHARACTER_HERHIS_HER
		KnowledgeGodHerHim = CHARACTER_HERHIM_HER

		#WarGod
		WarGodName = khetism_war_god_name
		WarGodNamePossessive = khetism_war_god_name_possessive
		WarGodSheHe = CHARACTER_SHEHE_SHE
		WarGodHerHis = CHARACTER_HERHIS_HER
		WarGodHerHim = CHARACTER_HERHIM_HER

		#TricksterGod
		TricksterGodName = khetism_trickster_god_name
		TricksterGodNamePossessive = khetism_trickster_god_name_possessive
		TricksterGodSheHe = CHARACTER_SHEHE_HE
		TricksterGodHerHis = CHARACTER_HERHIS_HIS
		TricksterGodHerHim = CHARACTER_HERHIM_HIM

		#NightGod
		NightGodName = khetism_night_god_name
		NightGodNamePossessive = khetism_night_god_name_possessive
		NightGodSheHe = CHARACTER_SHEHE_SHE
		NightGodHerHis = CHARACTER_HERHIS_HER
		NightGodHerHim = CHARACTER_HERHIM_HER

		#WaterGod
		WaterGodName = khetism_water_god_name
		WaterGodNamePossessive = khetism_water_god_name_possessive
		WaterGodSheHe = CHARACTER_SHEHE_SHE
		WaterGodHerHis = CHARACTER_HERHIS_HER
		WaterGodHerHim = CHARACTER_HERHIM_HER


		PantheonTerm = religion_the_gods
		PantheonTermHasHave = pantheon_term_have
		GoodGodNames = {
			khetism_high_god_name
		}
		
		
		DevilName = khetism_devil_name
		DevilNamePossessive = khetism_devil_name_possessive
		DevilSheHe = khetism_devil_shehe
		DevilHerHis = khetism_devil_herhis
		DevilHerselfHimself = khetism_devil_herselfhimself
		EvilGodNames = {
			khetism_devil_name
		}
		HouseOfWorship = khetism_house_of_worship
		HouseOfWorshipPlural = khetism_house_of_worship_plural
		ReligiousSymbol = khetism_religious_symbol
		ReligiousText = khetism_religious_text
		ReligiousHeadName = khetism_religious_head_title
		ReligiousHeadTitleName = khetism_religious_head_title_name
		DevoteeMale = khetism_devotee_male
		DevoteeMalePlural = khetism_devotee_male_plural
		DevoteeFemale = khetism_devotee_female
		DevoteeFemalePlural = khetism_devotee_female_plural
		DevoteeNeuter = khetism_devotee_neuter
		DevoteeNeuterPlural = khetism_devotee_neuter_plural
		PriestMale = khetism_priest
		PriestMalePlural = khetism_priest_plural
		PriestFemale = khetism_priest
		PriestFemalePlural = khetism_priest_plural
		PriestNeuter = khetism_priest
		PriestNeuterPlural = khetism_priest_plural
		AltPriestTermPlural = khetism_priest_term_plural
		BishopMale = khetism_bishop
		BishopMalePlural = khetism_bishop_plural
		BishopFemale = khetism_bishop
		BishopFemalePlural = khetism_bishop_plural
		BishopNeuter = khetism_bishop
		BishopNeuterPlural = khetism_bishop_plural
		DivineRealm = khetism_divine_realm
		PositiveAfterLife = khetism_positive_afterlife
		NegativeAfterLife = khetism_negative_afterlife
		DeathDeityName = khetism_death_name
		DeathDeityNamePossessive = khetism_death_name_possessive
		DeathDeitySheHe = CHARACTER_SHEHE_HE
		DeathDeityHerHis = CHARACTER_HERHIS_HIS
		DeathDeityHerHim = CHARACTER_HERHIM_HIM

		GHWName = ghw_purification
		GHWNamePlural = ghw_purifications
	}

	faiths = {
		khetarchy = {
			#color = { 252 162 100 }
			color = { 247 127 46 }
			icon = khetarchy
			
			holy_site = kheterat
			holy_site = nirat
			holy_site = golkora
			holy_site = ibtat
			#holy_site = mothers_tears

			doctrine = tenet_pentarchy
			doctrine = tenet_adaptive
			doctrine = tenet_communal_identity
		}

	}
}