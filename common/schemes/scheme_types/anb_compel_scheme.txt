﻿
anb_compel_spell = {
	# Basic Setup
	skill = learning
	desc = anb_compel_desc_general
	success_desc = "anb_compel_success_desc"
	discovery_desc = "anb_compel_discovery_desc"
	icon = icon_scheme_fabricate_hook # TODO
	illustration = "gfx/interface/illustrations/event_scenes/corridor.dds" # TODO
	category = hostile
	target_type = character
	is_secret = yes
	maximum_breaches = 5
	cooldown = { years = 5 }

	# Parameters
	speed_per_skill_point = 5
	speed_per_target_skill_point = 0
	spymaster_speed_per_skill_point = 0
	target_spymaster_speed_per_skill_point = 0
	tier_speed = 1	
	base_progress_goal = 100
	maximum_secrecy = 95
	base_maximum_success = 95
	phases_per_agent_charge = 1
	success_chance_growth_per_skill_point = t2_scgpsp_value
	
	# Core Triggers
	allow = {
		is_adult = yes		
		is_playable_character = yes
		is_imprisoned = no
	}
	valid = {
		has_magical_affinity = yes
	}
	
	# Agents
	agent_leave_threshold = 0
	agent_join_chance = {
		base = 0
	}
	valid_agent = { always = no }
	
	base_success_chance = {
		base = 10

		# CASTER #
		#Magical affinity level
		modifier = {
			add = 20
			scope:owner = { has_trait = magical_affinity_2 }
		}
		modifier = {
			add = 40
			scope:owner = { has_trait = magical_affinity_3 }
		}
		#How well do they know the target
		modifier = {
			add = 10
			scope:owner = { has_relation_friend = scope:target }
		}
		modifier = {
			add = 10
			scope:owner = { has_relation_soldier_friend = scope:target }
		}
		modifier = {
			add = 20
			scope:owner = { has_relation_best_friend = scope:target }
		}
		modifier = {
			add = 10
			scope:owner = { has_relation_lover = scope:target }
		}
		modifier = {
			add = 20
			scope:owner = { has_relation_soulmate = scope:target }
		}
		
		# TARGET #
		modifier = {
			add = -10
			scope:owner = {	has_trait = calm }
		}
		modifier = {
			add = -20
			scope:owner = {	has_trait = stubborn }
		}
		modifier = {
			add = 20
			scope:owner = { has_trait = fickle }
		}
		
		# DISCOVERY #
		modifier = {
			add = -30
			always = scope:exposed
			desc = "SCHEME_IS_EXPOSED"
		}
	}
	base_secrecy = {
		add = secrecy_base_value
		add = countermeasure_apply_secrecy_maluses_value
	}
	
	# On Actions
	on_start = {
		scheme_owner = { trigger_event =  anb_compel_outcome.1 }
	}
	on_phase_completed = {
	}
	on_hud_click  = {
	}
	on_monthly = {
		hostile_scheme_monthly_discovery_chance_effect = yes
		if = {
			limit = {
				NOT = { exists = scope:discovery_event_happening }
			}
			scheme_owner = {
				trigger_event = {
					on_action = anb_compel_ongoing
					days = { 1 7 }
				}
			}
		}
	}
	on_semiyearly = {
	}
	on_invalidated = {
		scheme_target_character = {
			save_scope_as = target
		}
		scheme_owner = {
			save_scope_as = owner
		}
		
		# Anbennar TODO: make these events into send_interface_toast
		if = {
			limit = { NOT = { scope:target.liege_or_court_owner = scope:owner.liege_or_court_owner } }
			scope:owner = { trigger_event = anb_spell_interruption.1 }
		}
		else_if = {
			limit = { 
				scope:target = {
					NAND = {
						is_imprisoned = no
						is_imprisoned_by = scope:owner
					}
				} 		
			}
			scope:owner = { trigger_event = anb_spell_interruption.2 }
		}
		else_if = {
			limit = { scope:target = { is_alive = no } }
			scope:owner = { trigger_event = anb_spell_interruption.3 }
		}
		else_if = {
			limit = { scope:target = { has_trait = incapable } }
			scope:owner = { trigger_event = anb_spell_interruption.4 }
		}
		else_if = {
			limit = { scope:owner = { has_weak_hook = scope:target } }
			scope:owner = { trigger_event = anb_spell_interruption.10 }
		}
		else_if = {
			limit = { scope:owner = { has_strong_hook = scope:target } }
			scope:owner = { trigger_event = anb_spell_interruption.11 }
		}
	}
}
