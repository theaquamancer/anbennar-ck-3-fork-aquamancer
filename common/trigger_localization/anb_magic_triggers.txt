﻿### General Magic ###

anb_in_spell_range = {
	global = anb_in_spell_range_global
	global_not = anb_in_spell_range_global
	first = anb_in_spell_range_global
	first_not = anb_in_spell_range_global
}

### Magic Lifestyle ###
magic_lifestyle_perk_points = {
	global = MAGIC_LIFESTYLE_PERK_POINTS_TRIGGER
}