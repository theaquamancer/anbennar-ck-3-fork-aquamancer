﻿#Events for race maintenance

namespace = racial

# 0001 - Assigns characters without a racial trait a trait according to their culture

#Assigns characters without a racial trait a trait according to their culture
racial.0001 = {
	hidden = yes
	
	trigger = {
		exists = this
		exists = this.culture
		NOT = {
			has_racial_trait = yes
		}
	}
	
	immediate = {
		assign_racial_trait_effect = yes
	}
}

#Assigns one racial trait if child is result of two different races
racial.1001 = {
	hidden = yes
	
	immediate = {
		#Elf-race_human-orc group
		if = {
			limit = {
				has_trait = race_elf
				OR = {
					has_trait = race_half_elf
					has_trait = race_human
					has_trait = race_half_orc
				}
			}
			remove_trait = race_elf
			remove_trait = race_human
			remove_trait = race_half_orc
			add_trait = race_half_elf
			if = {
				limit = {
					scope:mother = {
						is_human_culture = yes
					}
					OR = {
						NOT = { exists = scope:father }
						scope:father = {
							NOT = { is_human_culture = yes }
						}
					}
				}
				set_culture_same_as = scope:mother
			}
			else_if = {
				limit = {
					exists = scope:father
					scope:father = {
						is_human_culture = yes
					}
					scope:mother = {
						NOT = { is_human_culture = yes }
					}
				}
				set_culture_same_as = scope:father
			}
		}
		else_if = {
			limit = {
				has_trait = race_orc
				OR = {
					has_trait = race_elf
					has_trait = race_half_elf
					has_trait = race_human
					has_trait = race_half_orc
				}
			}
			remove_trait = race_elf
			remove_trait = race_half_elf
			remove_trait = race_human
			remove_trait = race_orc
			add_trait = race_half_orc
		}
		else_if = {
			limit = {
				has_trait = race_half_elf
				has_trait = race_half_orc
			}
			random_list = {
				75 = {
					remove_trait = race_half_elf
				}
				25 = {
					remove_trait = race_half_orc
				}
			}
		}
		else_if = {
			limit = {
				has_trait = race_half_elf
				has_trait = race_human
			}
			random_list = {
				50 = {
					remove_trait = race_half_elf
				}
				50 = {
					remove_trait = race_human
				}
			}
		}
		else_if = {
			limit = {
				has_trait = race_half_orc
				has_trait = race_human
			}
			random_list = {
				25 = {
					remove_trait = race_half_orc
				}
				75 = {
					remove_trait = race_human
				}
			}
		}
		#Mules
		else_if = {
			limit = {
				OR = {
					has_trait = race_elf
					has_trait = race_half_elf
					has_trait = race_human
					has_trait = race_orc
					has_trait = race_half_orc
				}
				has_trait = race_goblin
			}
			remove_trait = race_elf
			remove_trait = race_half_elf
			remove_trait = race_human
			remove_trait = race_orc
			remove_trait = race_half_orc
			remove_trait = race_goblin
			add_trait = race_half_goblin
		}
		else_if = {
			limit = {
				OR = {
					has_trait = race_elf
					has_trait = race_half_elf
					has_trait = race_human
					has_trait = race_orc
					has_trait = race_half_orc
				}
				has_trait = race_hobgoblin
			}
			remove_trait = race_elf
			remove_trait = race_half_elf
			remove_trait = race_human
			remove_trait = race_orc
			remove_trait = race_half_orc
			remove_trait = race_hobgoblin
			add_trait = race_half_hobgoblin
		}
		else_if = {
			limit = {
				has_trait = race_halfling
				has_trait = race_gnome
			}
			remove_trait = race_halfling
			remove_trait = race_gnome
			add_trait = race_gnomeling
		}
		else_if = {
			limit = {
				has_trait = race_troll
				has_trait = race_ogre
			}
			remove_trait = race_troll
			remove_trait = race_ogre
			add_trait = race_trollkin
		}
		#Light-mules
		else_if = {
			limit = {
				has_trait = race_orc
				has_trait = race_ogre
			}
			remove_trait = race_orc
			remove_trait = race_ogre
			add_trait = race_ogrillon
		}
		else_if = {
			limit = {
				has_trait = race_orc
				has_trait = race_ogrillon
			}
			random_list = {
				90 = {
					remove_trait = race_orc
				}
				10 = {
					remove_trait = race_ogrillon
				}
			}
		}
		else_if = {
			limit = {
				has_trait = race_ogre
				has_trait = race_ogrillon
			}
			random_list = {
				90 = {
					remove_trait = race_ogre
				}
				10 = {
					remove_trait = race_ogrillon
				}
			}
		}
		else_if = {
			limit = {
				has_trait = race_goblin
				has_trait = race_hobgoblin
			}
			remove_trait = race_goblin
			remove_trait = race_hobgoblin
			add_trait = race_lesser_hobgoblin
		}
		else_if = {
			limit = {
				has_trait = race_goblin
				has_trait = race_lesser_hobgoblin
			}
			random_list = {
				90 = {
					remove_trait = race_goblin
				}
				10 = {
					remove_trait = race_lesser_hobgoblin
				}
			}
		}
		else_if = {
			limit = {
				has_trait = race_hobgoblin
				has_trait = race_lesser_hobgoblin
			}
			random_list = {
				90 = {
					remove_trait = race_hobgoblin
				}
				10 = {
					remove_trait = race_lesser_hobgoblin
				}
			}
		}
		#Harpy
		else_if = {
			limit = {
				has_trait = race_harpy
			}
			remove_trait = race_elf
			remove_trait = race_half_elf
			remove_trait = race_human
			remove_trait = race_orc
			remove_trait = race_half_orc
			remove_trait = race_goblin
			remove_trait = race_hobgoblin
		}
	}
}

#If assumed father's race cannot produce child's race expose bastard: father exposes
racial.1011 = {
	type = character_event
	title = racial.1011.t
	desc = {
		first_valid = {
			triggered_desc = {
				trigger = {
					scope:child = {
						has_trait = twin
					}
				}
				desc = racial.1011.desc.twin
			}
			desc = racial.1011.desc
		}
	}
	theme = bastardy
	left_portrait = {
		character = scope:father
		animation = disapproval
	}
	lower_left_portrait = {
		character = scope:child
	}
	right_portrait = {
		character = scope:mother
		animation = shame
	}
	lower_right_portrait = {
		character = scope:real_father
		trigger = {
			exists = scope:real_father
		}
	}
	
	trigger = {
		OR = {
			scope:child = {
				NOT = {
					assumed_father_matches_child_race = {
						MOTHER = scope:mother
						FATHER = scope:father
						CHILD = scope:child
					}
				}
			}
			AND = {
				exists = scope:child_2
				scope:child_2 = {
					NOT = {
						assumed_father_matches_child_race = {
							MOTHER = scope:mother
							FATHER = scope:father
							CHILD = scope:child_2
						}
					}
				}
			}
		}
	}

	immediate = {
		scope:mother = {
			every_secret = {
				limit = {
					secret_type = secret_disputed_heritage
					secret_target = scope:child
				}
				save_scope_as = secret
			}
			if = {
				limit = {
					exists = scope:child_2
				}
				every_secret = {
					limit = {
						secret_type = secret_disputed_heritage
						secret_target = scope:child_2
					}
					save_scope_as = secret_2
				}
			}
		}
		if = { # Anbennar: for the log
			limit = {
				exists = scope:secret
			}
			scope:secret = {
				reveal_to_without_events_effect = {
					CHARACTER = root
				}
			}
		}
		if = { # Anbennar: for the log
			limit = {
				exists = scope:secret_2
			}
			scope:secret_2 = {
				reveal_to_without_events_effect = {
					CHARACTER = root
				}
			}
		}
	}

	option = { #Expose!
		name = racial.1011.a
		scope:secret = {
			expose_secret = root
		}
		scope:child = {
			set_father = scope:real_father
			remove_trait = disputed_heritage
			add_bastard_trait_based_on_faith_effect = yes
			set_parent_house_effect = yes
		}
		if = {
			limit = {
				exists = scope:child_2
			}
			scope:secret_2 = {
				expose_secret = root
			}
			scope:child_2 = {
				set_father = scope:real_father
				remove_trait = disputed_heritage
				add_bastard_trait_based_on_faith_effect = yes
				set_parent_house_effect = yes
			}
		}
	}
}

#If assumed father's race cannot produce child's race expose bastard: inform mother
racial.1012 = {
	type = character_event
	title = racial.1011.t
	desc = {
		first_valid = {
			triggered_desc = {
				trigger = {
					scope:child = {
						has_trait = twin
					}
				}
				desc = racial.1012.desc.twin
			}
			desc = racial.1012.desc
		}
	}
	theme = bastardy
	left_portrait = {
		character = scope:father
		animation = disapproval
	}
	lower_left_portrait = {
		character = scope:child
	}
	right_portrait = {
		character = scope:mother
		animation = shame
	}
	lower_right_portrait = {
		character = scope:real_father
		trigger = {
			exists = scope:real_father
		}
	}
	
	trigger = {
		OR = {
			scope:child = {
				NOT = {
					assumed_father_matches_child_race = {
						MOTHER = scope:mother
						FATHER = scope:father
						CHILD = scope:child
					}
				}
			}
			AND = {
				exists = scope:child_2
				scope:child_2 = {
					NOT = {
						assumed_father_matches_child_race = {
							MOTHER = scope:mother
							FATHER = scope:father
							CHILD = scope:child_2
						}
					}
				}
			}
		}
	}

	immediate = {
		scope:mother = {
			every_secret = {
				limit = {
					secret_type = secret_disputed_heritage
					secret_target = scope:child
				}
				save_scope_as = secret
			}
			if = {
				limit = {
					exists = scope:child_2
				}
				every_secret = {
					limit = {
						secret_type = secret_disputed_heritage
						secret_target = scope:child_2
					}
					save_scope_as = secret_2
				}
			}
		}
		if = { # Anbennar: for the log
			limit = {
				exists = scope:secret
			}
			scope:secret = {
				reveal_to_without_events_effect = {
					CHARACTER = root
				}
			}
		}
		if = { # Anbennar: for the log
			limit = {
				exists = scope:secret_2
			}
			scope:secret_2 = {
				reveal_to_without_events_effect = {
					CHARACTER = root
				}
			}
		}
	}

	option = { #Expose!
		name = racial.1012.a
		show_as_tooltip = {
			scope:father = {
				scope:secret = {
					expose_secret = scope:father
				}
				scope:child = {
					set_father = scope:real_father
					remove_trait = disputed_heritage
					add_bastard_trait_based_on_faith_effect = yes
					set_parent_house_effect = yes
				}
				if = {
					limit = {
						exists = scope:child_2
					}
					scope:secret_2 = {
						expose_secret = scope:father
					}
					scope:child_2 = {
						set_father = scope:real_father
						remove_trait = disputed_heritage
						add_bastard_trait_based_on_faith_effect = yes
						set_parent_house_effect = yes
					}
				}
			}
		}
	}
}

#Kills male harpy children
racial.2001 = {
	type = character_event
	title = racial.2001.t
	desc = racial.2001.desc
	theme = death
	left_portrait = scope:child
	right_portrait = scope:child_2

	immediate = {
		play_music_cue = death

		if = {
			limit = {
				exists = scope:father
				scope:father = {
					is_alive = yes
				}
			}
			scope:father = {
				add_character_flag = {
					flag = sent_relevant_death_event
					days = 5
				}
				trigger_event = racial.2002
			}
		}
		scope:child = {
			if = {
				limit = {
					is_male = yes
				}
				death = {
					death_reason = death_delivery
				}
			}
		}
		scope:child_2 = {
			if = {
				limit = {
					is_male = yes
				}
				death = {
					death_reason = death_delivery
				}
			}
		}
	}

	option = {
		name = racial.2001.a
	}
}

#Kills male harpy children: inform father
racial.2002 = {
	type = character_event
	title = racial.2001.t
	desc = racial.2001.desc
	theme = death
	right_portrait = scope:child
	lower_right_portrait = scope:child_2

	immediate = {
		play_music_cue = death

		show_as_tooltip = {
			scope:child = {
				if = {
					limit = {
						is_male = yes
					}
					death = {
						death_reason = death_delivery
					}
				}
			}
			scope:child_2 = {
				if = {
					limit = {
						is_male = yes
					}
					death = {
						death_reason = death_delivery
					}
				}
			}
		}
	}

	option = {
		name = racial.2001.a
	}
}