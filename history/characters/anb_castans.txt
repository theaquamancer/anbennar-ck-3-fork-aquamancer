﻿
# Castans

castan_I = { # Castan I the Progenitor
	name = "Castan"
	# dynasty =
	religion = castanorian_pantheon
	culture = castanite
	
	trait = race_human
	
	-668.1.1 = { # right year, random date PLACEHOLDER
		birth = yes
	}
	
	-621.1.1 = { # right year, random date PLACEHOLDER
		give_nickname = nick_the_progenitor
		death = {
			death_reason = death_disappearance # Disappeared during the Battle of the White Flame
		}
	}
}

castan_II = { # Castan II Beastbane
	name = "Castan"
	# dynasty =
	religion = castanorian_pantheon
	culture = castanite
	
	trait = race_human
	
	-630.1.1 = { # random date PLACEHOLDER
		birth = yes
	}
	
	-569.1.1 = { # right year, random date PLACEHOLDER
		give_nickname = nick_beastbane
		death = {
			death_reason = death_disappearance # Disappeared along with most of the Beastbane Legion allegedly after entering the Gates of Hell
		}
	}
}

castan_III = { # Castan III Dwarf-friend
	name = "Castan"
	# dynasty =
	religion = castanorian_pantheon
	culture = castanite
	
	trait = race_human
	
	-575.1.1 = { # random date PLACEHOLDER
		birth = yes
	}
	
	-521.1.1 = { # right year, random date PLACEHOLDER
		give_nickname = nick_dwarf_friend
		death = "-521.1.1"
	}
}

castan_IV = { # Castan IV Realmbuilder
	name = "Castan"
	# dynasty =
	religion = castanorian_pantheon
	culture = castanite
	
	trait = race_human
	
	-550.1.1 = { # random date PLACEHOLDER
		birth = yes
	}
	
	-471.1.1 = { # right year, random date PLACEHOLDER
		give_nickname = nick_realmbuilder
		death = "-471.1.1"
	}
}

castan_V = { # Castan V the Great
	name = "Castan"
	# dynasty =
	religion = castanorian_pantheon
	culture = castanite
	
	trait = race_human
	
	-500.1.1 = { # random date PLACEHOLDER
		birth = yes
	}
	
	-420.1.1 = { # right year, random date PLACEHOLDER
		give_nickname = nick_the_great
		death = "-420.1.1"
	}
}

castan_VI = { # Castan VI Giantsbane
	name = "Castan"
	# dynasty =
	religion = castanorian_pantheon
	culture = castanite
	
	trait = race_human
	
	-438.1.1 = { # random date PLACEHOLDER
		birth = yes
	}
	
	-357.1.1 = { # right year, random date PLACEHOLDER
		give_nickname = nick_giantsbane
		death = "-357.1.1"
	}
}

castan_VII = { # Castan VII the Preserver
	name = "Castan"
	# dynasty =
	religion = castanorian_pantheon
	culture = castanite
	
	trait = race_human
	
	-375.1.1 = { # random date PLACEHOLDER
		birth = yes
	}
	
	-307.1.1 = { # right year, random date PLACEHOLDER
		give_nickname = nick_the_preserver
		death = "-307.1.1"
	}
}

castan_VIII = { # Castan VIII
	name = "Castan"
	# dynasty =
	religion = castanorian_pantheon
	culture = castanite
	
	trait = race_human
	
	-350.1.1 = { # random date PLACEHOLDER
		birth = yes
	}
	
	-302.1.1 = { # right year, random date PLACEHOLDER
		death = "-302.1.1"
	}
}

castan_IX = { # Castan IX Peacemaker
	name = "Castan"
	# dynasty =
	religion = castanorian_pantheon
	culture = castanite
	
	trait = race_human
	
	-334.1.1 = { # random date PLACEHOLDER
		birth = yes
	}
	
	-294.1.1 = { # right year, random date PLACEHOLDER
		give_nickname = nick_the_peacemaker
		death = "-294.1.1"
	}
}

castan_X = { # Castan X the Quareller
	name = "Castan"
	# dynasty =
	religion = castanorian_pantheon
	culture = castanite
	
	trait = race_human
	
	-315.1.1 = { # random date PLACEHOLDER
		birth = yes
	}
	
	-248.1.1 = { # right year, random date PLACEHOLDER
		give_nickname = nick_the_quarreller
		death = {
			death_reason = death_battle # A Warmonger, killed in battle in his 2nd war against Dameria.
		}
	}
}

castan_XI = { # Castan XI the Righteous
	name = "Castan"
	# dynasty =
	religion = castanorian_pantheon
	culture = castanite
	
	trait = race_human
	
	-290.1.1 = { # random date PLACEHOLDER
		birth = yes
	}
	
	-244.1.1 = { # right year, random date PLACEHOLDER
		death = "-244.1.1"
	}
}

castan_XII = { # Castan XII the Old
	name = "Castan"
	# dynasty =
	religion = castanorian_pantheon
	culture = castanite
	
	trait = race_human
	
	-269.1.1 = { # random date PLACEHOLDER
		birth = yes
	}
	
	-144.1.1 = { # right year, random date PLACEHOLDER
		give_nickname = nick_the_old
		death = {
			death_reason = death_old_age # he died at 125
		}
	}
}

castan_XIII = { # Castan XIII
	name = "Castan"
	# dynasty =
	religion = castanorian_pantheon
	culture = castanite
	
	trait = race_human
	
	-175.1.1 = { # random date PLACEHOLDER
		birth = yes
	}
	
	-132.1.1 = { # right year, random date PLACEHOLDER
		death = "-132.1.1"
	}
}

castan_XIV = { # Castan XIV
	name = "Castan"
	# dynasty =
	religion = castanorian_pantheon
	culture = castanite
	
	trait = race_human
	
	-150.1.1 = { # random date PLACEHOLDER
		birth = yes
	}
	
	-111.1.1 = { # right year, random date PLACEHOLDER
		death = "-111.1.1"
	}
}

castan_XV = { # Castan XV the Short
	name = "Castan"
	# dynasty =
	religion = castanorian_pantheon
	culture = castanite
	
	trait = race_human
	
	-140.1.1 = { # random date PLACEHOLDER
		birth = yes
	}
	
	-111.1.1 = { # right year, random date PLACEHOLDER
		# shortest reign
		death = "-111.1.1"
	}
}

castan_XVI = { # Castan XVI
	name = "Castan"
	# dynasty =
	religion = castanorian_pantheon
	culture = castanite
	
	trait = race_human
	
	-130.1.1 = { # random date PLACEHOLDER
		birth = yes
	}
	
	-83.1.1 = { # right year, random date PLACEHOLDER
		death = "-83.1.1"
	}
}

castan_XVII = { # Castan XVII
	name = "Castan"
	# dynasty =
	religion = castanorian_pantheon
	culture = castanite
	
	trait = race_human
	
	-100.1.1 = { # random date PLACEHOLDER
		birth = yes
	}
	
	-70.1.1 = { # right year, random date PLACEHOLDER
		death = "-70.1.1"
	}
}

castan_XVIII = { # Castan XVIII
	name = "Castan"
	# dynasty =
	religion = castanorian_pantheon
	culture = castanite
	
	trait = race_human
	
	-90.1.1 = { # random date PLACEHOLDER
		birth = yes
	}
	
	-59.1.1 = { # right year, random date PLACEHOLDER
		death = "-59.1.1"
	}
}

castan_XIX = { # Castan XIX
	name = "Castan"
	# dynasty =
	religion = castanorian_pantheon
	culture = castanite
	
	trait = race_human
	
	-80.1.1 = { # random date PLACEHOLDER
		birth = yes
	}
	
	-33.1.1 = { # right year, random date PLACEHOLDER
		death = "-33.1.1"
	}
}

castan_XX = { # Castan XX the Youthful
	name = "Castan"
	# dynasty =
	religion = castanorian_pantheon
	culture = castanite
	
	trait = race_human
	
	-55.1.1 = { # random date PLACEHOLDER
		birth = yes
	}
	
	-30.1.1 = { # right year, random date PLACEHOLDER
		death = "-30.1.1"
	}
}

castan_XXI = { # Castan XXI the Compassionate
	name = "Castan"
	# dynasty =
	religion = castanorian_pantheon
	culture = castanite
	
	trait = race_human
	trait = compassionate
	
	-50.1.1 = { # random date PLACEHOLDER
		birth = yes
	}
	
	-27.1.1 = { # right year, random date PLACEHOLDER
		give_nickname = nick_the_compassionate
		death = "-27.1.1"
	}
}

castan_XXII = { # Castan XXII the Eccentric
	name = "Castan"
	# dynasty =
	religion = castanorian_pantheon
	culture = castanite
	
	trait = race_human
	
	-50.1.1 = { # random date PLACEHOLDER
		birth = yes
	}
	
	-26.1.1 = { # right year, random date PLACEHOLDER
		give_nickname = nick_the_eccentric
		death = "-26.1.1"
	}
}

castan_XXIII = { # Castan XXIII
	name = "Castan"
	# dynasty =
	religion = castanorian_pantheon
	culture = castanite
	
	trait = race_human
	
	-50.1.1 = { # random date PLACEHOLDER
		birth = yes
	}
	
	-13.1.1 = { # right year, random date PLACEHOLDER
		death = "-13.1.1"
	}
}

castan_XXIV = { # Castan XXIV the Unready
	name = "Castan"
	# dynasty =
	religion = castanorian_pantheon
	culture = castanite
	
	trait = race_human
	
	-21.1.1 = { # random date PLACEHOLDER
		birth = yes
	}
	
	4.1.1 = { # right year, random date PLACEHOLDER
		give_nickname = nick_the_unready
		# Killed by a Damestear meteorite crashing into the North Citadel.
		death = "4.1.1"
	}
}

castan_XXV = { # Castan XXV the Perished
	name = "Castan"
	# dynasty =
	religion = castanorian_pantheon
	culture = castanite
	
	trait = race_human
	
	-30.1.1 = { # random date PLACEHOLDER
		birth = yes
	}
	
	5.1.1 = { # right year, random date PLACEHOLDER
		give_nickname = nick_the_perished
		# Killed by Damestear meteorite the night before a battle
		death = "31.1.1"
	}
}

castan_XXVI = { # Castan XXVI the Unwavering
	name = "Castan"
	# dynasty =
	religion = castanorian_pantheon
	culture = castanite
	
	trait = race_human
	
	-10.1.1 = { # random date PLACEHOLDER
		birth = yes
	}
	
	43.1.1 = { # right year, random date PLACEHOLDER
		give_nickname = nick_the_unwavering
		death = "31.1.1"
	}
}

castan_XXVII = { # Castan XXVII the Sickly
	name = "Castan"
	# dynasty =
	religion = castanorian_pantheon
	culture = castanite
	
	trait = race_human
	
	20.1.1 = { # random date PLACEHOLDER
		birth = yes
	}
	
	101.1.1 = { # right year, random date PLACEHOLDER
		give_nickname = nick_the_sickly
		death = "101.1.1"
	}
}

castan_XXVIII = { # Castan XXVIII
	name = "Castan"
	# dynasty =
	religion = castanorian_pantheon
	culture = castanite
	
	trait = race_human
	
	80.1.1 = { # random date PLACEHOLDER
		birth = yes
	}
	
	118.1.1 = { # right year, random date PLACEHOLDER
		death = "118.1.1"
	}
}

castan_XXIX = { # Castan XXIX
	name = "Castan"
	# dynasty =
	religion = castanorian_pantheon
	culture = castanite
	
	trait = race_human
	
	100.1.1 = { # random date PLACEHOLDER
		birth = yes
	}
	
	157.1.1 = { # right year, random date PLACEHOLDER
		death = "157.1.1"
	}
}

castan_XXX = { # Castan XXX
	name = "Castan"
	# dynasty =
	religion = castanorian_pantheon
	culture = castanite
	
	trait = race_human
	
	140.1.1 = { # random date PLACEHOLDER
		birth = yes
	}
	
	201.1.1 = { # right year, random date PLACEHOLDER
		death = "201.1.1"
	}
}

castan_XXXI = { # Castan XXXI the Venerable
	name = "Castan"
	# dynasty =
	religion = castanorian_pantheon
	culture = castanite
	
	trait = race_human
	
	185.1.1 = { # random date PLACEHOLDER
		birth = yes
	}
	
	307.1.1 = { # right year, random date PLACEHOLDER
		give_nickname = nick_the_venerable
		death = {
			death_reason = death_old_age
		}
	}
}

castan_XXXII = { # Castan XXXII
	name = "Castan"
	# dynasty =
	religion = castanorian_pantheon
	culture = castanite
	
	trait = race_human
	
	280.1.1 = { # random date PLACEHOLDER
		birth = yes
	}
	
	323.1.1 = { # right year, random date PLACEHOLDER
		death = "323.1.1"
	}
}

castan_XXXIII = { # Castan XXXIII
	name = "Castan"
	# dynasty =
	religion = castanorian_pantheon
	culture = castanite
	
	trait = race_human
	
	300.1.1 = { # random date PLACEHOLDER
		birth = yes
	}
	
	374.1.1 = { # right year, random date PLACEHOLDER
		death = "374.1.1"
	}
}

castan_XXXIV = { # Castan XXXIV
	name = "Castan"
	# dynasty =
	religion = castanorian_pantheon
	culture = castanite
	
	trait = race_human
	
	350.1.1 = { # random date PLACEHOLDER
		birth = yes
	}
	
	398.1.1 = { # right year, random date PLACEHOLDER
		death = "398.1.1"
	}
}

castan_XXXV = { # Castan XXXV the Defiant
	name = "Castan"
	# dynasty =
	religion = castanorian_pantheon
	culture = castanite
	
	trait = race_human
	trait = arrogant
	
	370.1.1 = { # random date PLACEHOLDER
		birth = yes
	}
	
	474.1.1 = { # right year, random date PLACEHOLDER
		give_nickname = nick_the_defiant
		death = "474.1.1" # Died in the Tragedy at Burnoll Field in Dragonwake.
	}
}

castan_XXXVI = { # Castan XXXVI the Dragonfriend
	name = "Castan"
	# dynasty =
	religion = castanorian_pantheon
	culture = castanite
	
	trait = race_human
	
	450.1.1 = { # random date PLACEHOLDER
		birth = yes
	}
	
	500.1.1 = { # right year, random date PLACEHOLDER
		give_nickname = nick_the_dragonfriend
		death = "500.1.1" # Died with the Avatar of Castellos while fighting Zaamalot
	}
}

castan_XXXVII = { # Castan XXXVII the Rebuilder
	name = "Castan"
	# dynasty =
	religion = castanorian_pantheon
	culture = castanite
	
	trait = race_human
	
	450.1.1 = { # random date PLACEHOLDER
		birth = yes
	}
	
	518.1.1 = { # right year, random date PLACEHOLDER
		give_nickname = nick_the_rebuilder
		death = "518.1.1"
	}
}

castan_XXXVIII = { # Castan XXXVIII the Humble
	name = "Castan"
	# dynasty =
	religion = castanorian_pantheon
	culture = castanite
	
	trait = race_human
	trait = humble
	
	500.1.1 = { # random date PLACEHOLDER
		birth = yes
	}
	
	566.1.1 = { # right year, random date PLACEHOLDER
		give_nickname = nick_the_humble
		death = "566.1.1"
	}
}

castan_XXXIX = { # Castan XXXIX the Silent
	name = "Castan"
	# dynasty =
	religion = castanorian_pantheon
	culture = castanite
	
	trait = race_human
	
	600.1.1 = { # random date PLACEHOLDER
		birth = yes
	}
	
	666.1.1 = { # right year, random date PLACEHOLDER
		give_nickname = nick_the_silent
		death = "666.1.1"
	}
}

castan_XL = { # Castan XL the Steward
	name = "Castan"
	# dynasty =
	religion = castanorian_pantheon
	culture = marcher
	
	trait = race_human
	
	610.1.1 = { # random date PLACEHOLDER
		birth = yes
	}
	
	669.1.1 = { # right year, random date PLACEHOLDER
		give_nickname = nick_the_steward
		death = "669.1.1" # killed during the daravan folly
	}
}

castan_XLI = { # Castan XLI the Vengeful
	name = "Castan"
	# dynasty =
	religion = castanorian_pantheon
	culture = castanite
	
	trait = race_human
	trait = vengeful
	
	651.1.1 = { # random date PLACEHOLDER
		birth = yes
	}
	
	700.1.1 = { # right year, random date PLACEHOLDER
		give_nickname = nick_the_vengeful
		death = "700.1.1"
	}
}

castan_XLII = { # Castan XLII the Cowardly
	name = "Castan"
	# dynasty =
	religion = castanorian_pantheon
	culture = castanite
	
	trait = race_human
	trait = craven
	
	673.1.1 = { # random date PLACEHOLDER
		birth = yes
	}
	
	707.1.1 = { # right year, random date PLACEHOLDER
		give_nickname = nick_the_cowardly
		death = "707.1.1"
	}
}

castan_XLIII = { # Castan XLIII
	name = "Castan"
	# dynasty =
	religion = castanorian_pantheon
	culture = castanite
	
	trait = race_human
	
	680.1.1 = { # random date PLACEHOLDER
		birth = yes
	}
	
	728.1.1 = { # right year, random date PLACEHOLDER
		death = "728.1.1"
	}
}

castan_XLIV = { # Castan XLIV
	name = "Castan"
	# dynasty =
	religion = castanorian_pantheon
	culture = castanite
	
	trait = race_human
	
	706.1.1 = { # random date PLACEHOLDER
		birth = yes
	}
	
	761.1.1 = { # right year, random date PLACEHOLDER
		death = "761.1.1"
	}
}

castan_XLV = { # Castan XLV
	name = "Castan"
	# dynasty =
	religion = castanorian_pantheon
	culture = castanite
	
	trait = race_human
	
	730.1.1 = { # random date PLACEHOLDER
		birth = yes
	}
	
	793.1.1 = { # right year, random date PLACEHOLDER
		death = "793.1.1"
	}
}

castan_XLVI = { # Castan XLVI
	name = "Castan"
	# dynasty =
	religion = castanorian_pantheon
	culture = castanite
	
	trait = race_human
	
	762.1.1 = { # random date PLACEHOLDER
		birth = yes
	}
	
	809.1.1 = { # right year, random date PLACEHOLDER
		death = "809.1.1"
	}
}

castan_XLVII = { # Castan XLVII Frostbane
	name = "Castan"
	# dynasty =
	religion = castanorian_pantheon
	culture = castanite
	
	trait = race_human
	
	785.1.1 = { # random date PLACEHOLDER
		birth = yes
	}
	
	859.1.1 = { # right year, random date PLACEHOLDER
		give_nickname = nick_frostbane
		death = "859.1.1"
	}
}

castan_XLVIII = { # Castan XLVIII
	name = "Castan"
	# dynasty =
	religion = castanorian_pantheon
	culture = castanite
	
	trait = race_human
	
	822.1.1 = { # random date PLACEHOLDER
		birth = yes
	}
	
	865.1.1 = { # right year, random date PLACEHOLDER
		death = "865.1.1"
	}
}

castan_XLIX = { # Castan XLIX Ebonfrost
	name = "Gunnar"
	dynasty = dynasty_ebonfrost
	religion = castanorian_pantheon
	culture = black_castanorian
	
	trait = race_human
	trait = reaver
	
	830.1.1 = { # random date PLACEHOLDER
		birth = yes
	}
	
	893.1.1 = { # right year, random date PLACEHOLDER
		name = "Castan"
		give_nickname = nick_ebonfrost
		death = "893.1.1"
	}
}

castan_L = { # Castan L
	name = "Castan"
	religion = castanorian_pantheon
	culture = castanite
	
	trait = race_human
	
	864.1.1 = { # random date PLACEHOLDER
		birth = yes
	}
	
	895.1.1 = { # right year, random date PLACEHOLDER
		death = "895.1.1"
	}
}

castan_LI = { # Castan LI
	name = "Castan"
	dynasty = dynasty_ebonfrost
	religion = castanorian_pantheon
	culture = black_castanorian
	
	trait = race_human
	
	father = castan_XLIX
	
	875.1.1 = { # random date PLACEHOLDER
		birth = yes
	}
	
	936.1.1 = { # right year, random date PLACEHOLDER
		death = "936.1.1"
	}
}

castan_LII = { # Castan LII the Kind
	name = "Castan"
	dynasty = dynasty_ebonfrost #Ebonfrost
	religion = castanorian_pantheon
	culture = black_castanorian
	
	trait = race_human
	trait = education_diplomacy_2
	trait = compassionate
	trait = content
	trait = lazy
	
	father = castan_LI
	
	901.10.4 = {
		birth = yes
	}
	
	936.1.1 = {
		give_nickname = nick_the_kind
	}
	
	978.1.5 = {	
		death = {	
			death_reason = death_possessed
			killer = the_sorcerer_king	#Nichmer
		}
	}
}

castan_LIII = { # Castan LIII the Enthralled
	name = "Castan"
	dynasty = dynasty_ebonfrost
	religion = castanorian_pantheon
	culture = black_castanorian
	
	trait = race_human
	trait = education_diplomacy_2
	trait = trusting
	trait = callous
	trait = arbitrary
	trait = possessed_1
	trait = weak
	
	father = 564
	
	955.1.29 = {
		birth = yes
	}
	
	972.2.24 = {
		add_spouse = 567	#Clothilde. She kills herself after he is enthralled
	}
	
	977.1.1 = {
		give_nickname = nick_the_enthralled	#becomes emperor 978.1.13
	}
	
	980.3.2 = {
		add_spouse = 568	#Onna. Executed as she was suspicious of enthrallment
	}
	
	1015.12.14 = {
		death = {
			death_reason = death_battle_of_balmire
			killer = the_sorcerer_king	#Sorcerer-King
		}
	}
}

the_sorcerer_king = {
	name = "Nichmer" #The Sorcerer-King
	religion = castanorian_pantheon
	culture = korbarid
	
	trait = race_human
	trait = education_learning_4
	trait = ambitious
	trait = patient
	trait = paranoid
	trait = callous
	trait = magical_affinity_3
	
	940.10.1 = {
		birth = yes
	}
	
	1015.12.14 = {		#Battle of Balmire
		give_nickname = nick_the_sorcerer_king
	}
	
	1020.10.31 = {	#Battle of Trialmount
		death = {
			death_reason = death_battle_of_trialmount
			killer = 34	#Clarimonde of Oldhaven
		}
	}
}
