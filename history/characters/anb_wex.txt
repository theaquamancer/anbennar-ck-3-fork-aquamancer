﻿ryonard_0001 = {
	name = "Dyren"
	dynasty = dynasty_ryonard #royal Wex house during WoTSK
	religion = "border_cults"
	culture = "wexonard"
	father = ryonard_0002	#Welham, the third son
	mother = ryonard_0006	#Anabel, a lowborn

	trait = race_human
	trait = legitimized_bastard
	
	1013.3.27 = {
		birth = yes
	}
	1013.3.27 = {
		designate_diarch = character:moonbeam0001
	}
}

ryonard_0002 = {
	name = "Welham"
	dynasty = dynasty_ryonard #royal Wex house during WoTSK
	religion = "border_cults"
	culture = "wexonard"
	father = ryonard_0005
	mother = carstenard_0004
	
	980.2.15 = {
		birth = yes
	}
	1014.2.16 = {	#Storming of Vertesk
		death = {	
			death_reason = death_killed_war_of_sorcerer_king
		}
	}
}

ryonard_0003 = {
	name = "Lothane"
	dynasty = dynasty_ryonard #royal Wex house during WoTSK
	religion = "border_cults"
	culture = "wexonard"
	father = ryonard_0005
	mother = carstenard_0004
	
	976.6.30 = {
		birth = yes
	}
	1014.2.16 = {	#Storming of Vertesk
		death = {	
			death_reason = death_killed_war_of_sorcerer_king
		}
	}
}

ryonard_0004 = {
	name = "Cedric"
	dynasty = dynasty_ryonard #royal Wex house during WoTSK
	religion = "border_cults"
	culture = "wexonard"
	father = ryonard_0005
	mother = carstenard_0004

	trait = education_martial_4
	
	971.2.14 = {
		birth = yes
	}
	989.6.30 = {	#Slaughter at Cronesford
		death = {
			death_reason = death_battle
		}
	}
}

ryonard_0005 = {
	name = "Ottomac"
	dynasty = dynasty_ryonard #royal Wex house during WoTSK
	religion = "border_cults"
	culture = "wexonard"

	father = ryonard_0009
	
	952.5.24 = {
		birth = yes
	}
	971.1.4 = {
		add_spouse = carstenard_0004
	}
	984.4.10 = {
		death = {
			death_reason = death_maimed
		}
	}
}

ryonard_0006 = {	#lowborn woman that Welham gets with during siege
	name = "Anabel"
	#dynasty = dynasty_ryonard #royal Wex house during WoTSK
	religion = "border_cults"
	culture = "wexonard"
	female = yes
	
	990.3.29 = {
		birth = yes
	}
	1010.9.27 = {	#to Dyren
		death = {
			death_reason = death_childbirth
		}
	}
}

ryonard_0007 = {
	name = "Ottomac"	#son of Lothane
	dynasty = dynasty_ryonard #royal Wex house during WoTSK
	religion = "border_cults"
	culture = "wexonard"

	father = ryonard_0003
	
	1000.12.10 = {
		birth = yes
	}
	1021.2.20 = {
		death = {
			death_reason = death_ill
		}
	}
}

ryonard_0008 = {
	name = "Mildrith"	#daughter of Lothane
	dynasty = dynasty_ryonard #royal Wex house during WoTSK
	religion = "border_cults"
	culture = "wexonard"

	female = yes

	father = ryonard_0003
	
	1007.6.16 = {
		birth = yes
	}
}

ryonard_0009 = {
	name = "Ryon"
	dynasty = dynasty_ryonard #royal Wex house during WoTSK
	religion = "border_cults"
	culture = "wexonard"

	father = ryonard_0010
	
	900.10.23 = {
		birth = yes
	}
	930.2.1 = {
		death = yes
	}
}

ryonard_0010 = {
	name = "Lothane"
	dynasty = dynasty_ryonard #royal Wex house during WoTSK
	religion = "border_cults"
	culture = "wexonard"

	father = ryonard_0011
	
	871.3.21 = {
		birth = yes
	}
	897.10.7 = {
		death = yes
	}
}

ryonard_0011 = {
	name = "Ryon"
	dynasty = dynasty_ryonard #royal Wex house during WoTSK
	religion = "border_cults"
	culture = "wexonard"
	
	836.6.1 = {
		birth = yes
	}
	892.4.7 = {
		death = yes
	}
}


vinerick_0001 = {
	name = "Crovan"
	dynasty = dynasty_vinerick
	culture = wexonard
	religion = border_cults
	
	trait = race_human
	trait = education_martial_2
	trait = arbitrary
	trait = callous
	trait = arrogant
	trait = magical_affinity_2
	
	972.8.8 = {
		birth = yes
	}
	
	979.1.1 = {
		effect = {
			set_relation_guardian = character:514 # Iacob the Betrayer
		}
	}
	
	988.8.8 = {
		effect = {
			remove_relation_guardian = character:514 # Iacob the Betrayer
		}
	}
	
	995.11.12 = {
		effect = {
			set_relation_lover = character:vinerick_0002
		}
	}
	
	1006.4.29 = {
		add_spouse = vinerick_0002
		effect = {
			set_relation_rival = character:514 # Iacob the Betrayer
		}
	}
	
	1022.1.1 = {
		effect = {
			set_primary_title_to = title:c_vinerick
		}
	}
}

vinerick_0002 = {
	name = "Lorinne" 
	# lowborn
	culture = castanorian
	religion = border_cults
	female = yes
	
	trait = race_human
	trait = education_learning_1
	trait = forgiving
	trait = arbitrary
	trait = trusting
	
	978.3.27 = {
		birth = yes
	}
	
	1006.4.29 = {
		add_spouse = vinerick_0001
	}
}

vinerick_0003 = {
	name = "Ottolin"
	dynasty = dynasty_vinerick
	culture = wexonard
	religion = border_cults
	
	trait = race_human
	trait = pensive
	trait = arbitrary
	trait = calm
	trait = cynical
	trait = magical_affinity_2
	
	father = vinerick_0001
	mother = vinerick_0002
	
	1007.1.8 = {
		birth = yes
	}
}

vinerick_0004 = {	#OG vinerick ruler
	name = "Alder"
	dynasty = dynasty_vinerick_old
	culture = wexonard
	religion = border_cults
	
	trait = race_human
	
	940.5.18 = {
		birth = yes
	}

	978.4.27 = {
		death = {
			death_reason = death_butchers_field_massacre
			killer = butcherson_0002	#Butcher's Field Massacre
		}
	}
}

butcherson_0001 = {	#OG vinerick ruler
	name = "Roderick"
	dynasty = dynasty_butcherson
	culture = wexonard
	religion = border_cults

	father = butcherson_0002
	
	trait = race_human

	trait = education_martial_3
	trait = paranoid
	trait = brave
	trait = just

	trait = wild_oat
	
	980.6.23 = {
		birth = yes
	}
}

butcherson_0002 = {
	name = "Elrac"
	dynasty = dynasty_butcherson
	culture = castanorian
	religion = castanorian_pantheon
	
	trait = race_human

	trait = education_martial_4
	trait = sadistic
	trait = wrathful
	trait = brave
	
	950.1.5 = {
		birth = yes
	}
	978.4.27 = { 	#Butcher's Field Massacre
		give_nickname = nick_the_butcher
	}	

	1006.4.23 = {	#Relief of Wexkeep
		death = {
			death_reason = death_battle
		}
	}
}

robacard_0001 = { # Hobert Robacard
	name = "Hobert"
	dynasty = dynasty_robacard
	culture = wexonard
	religion = border_cults

	trait = race_human
	trait = brave
	trait = stubborn
	trait = just
	trait = one_eyed
	trait = scarred
	trait = military_engineer
	trait = education_martial_3

	968.10.3 = {
		birth = yes
	}

	995.3.6 = {
		add_spouse = robacard_0002
	}

	1020.1.1 = {
		effect = {
			set_relation_rival = character:514 # Iacob the Betrayer
			set_relation_rival = character:vinerick_0001 # Crovan Vinerick
		}
	}
}

robacard_0002 = { # Mathilda
	name = "Mathilda"
	# lowborn
	culture = wexonard
	religion = border_cults
	female = yes

	trait = race_human
	trait = honest
	trait = gregarious
	trait = compassionate
	trait = education_stewardship_1

	966.12.3 = {
		birth = yes
	}

	995.3.6 = {
		add_spouse = robacard_0001
	}
}

robacard_003 = { # Aldric Robacard
	name = "Aldric"
	dynasty = dynasty_robacard
	culture = wexonard
	religion = border_cults

	trait = race_human
	trait = just
	trait = brave
	trait = gregarious
	trait = education_martial_2

	father = robacard_0001
	mother = robacard_0002

	997.5.11 = {
		birth = yes
	}

	1022.1.1 = {
		effect = {
			set_relation_friend = character:ryonard_0001 # Dyren Ryonard
		}
	}
}

carstenard_0001 = { # 
	name = "Ricard"
	dynasty = dynasty_carstenard
	culture = wexonard
	religion = border_cults

	trait = race_human
	trait = diligent
	trait = arrogant
	trait = deceitful
	trait = education_stewardship_4

	father = carstenard_0002
	mother = bronzewing_0005

	989.12.15 = {
		birth = yes
	}
}

carstenard_0002 = { # 
	name = "Carsten"
	dynasty = dynasty_carstenard
	culture = wexonard
	religion = border_cults

	trait = race_human
	trait = paranoid
	trait = patient
	trait = calm
	trait = education_intrigue_4

	trait = logistician

	father = carstenard_0003

	962.3.25 = {
		birth = yes
	}
	988.12.4 = {
		add_spouse = bronzewing_0005
	}
	1006.4.23 = {	#Relief of Wexkeep
		death = {
			death_reason = death_battle
		}
	}
}

carstenard_0003 = { # 
	name = "Admond"
	dynasty = dynasty_carstenard
	culture = wexonard
	religion = border_cults

	trait = race_human

	930.1.30 = {
		birth = yes
	}
	969.4.12 = {
		death = yes
	}
}

carstenard_0004 = { # 
	name = "Carwen"
	dynasty = dynasty_carstenard
	culture = wexonard
	religion = border_cults
	female = yes

	trait = race_human

	father = carstenard_0003

	958.2.12 = {
		birth = yes
	}
}

wincenard_0001 = { # 
	name = "Brandon"
	dynasty = dynasty_wincenard
	culture = wexonard
	religion = border_cults

	father = wincenard_0002

	trait = race_human
	trait = honest
	trait = temperate
	trait = stubborn
	trait = education_diplomacy_2

	1001.3.1 = {
		birth = yes
	}
}

wincenard_0002 = { # 
	name = "Alfred"
	dynasty = dynasty_wincenard
	culture = wexonard
	religion = border_cults

	trait = race_human

	father = wincenard_0003

	972.4.26 = {
		birth = yes
	}
	1014.5.14 = {
		death = {
			death_reason = death_battle_of_morban_flats
		}
	}
}

wincenard_0003 = { # 
	name = "Wincen"
	dynasty = dynasty_wincenard
	culture = wexonard
	religion = border_cults

	trait = race_human

	932.10.4 = {
		birth = yes
	}

	978.4.27 = {
		death = {
			death_reason = death_butchers_field_massacre
			killer = butcherson_0002	#Butcher's Field Massacre
		}
	}
}

rhinmond_0001 = {	#Rycroft of Sugamber, founder of sil Rhinmond cadet house of ruling Ryonard Wex
	name = "Rycroft"
	#dynasty = house_rhinmond #Sugambic
	dynasty_house = house_rhinmond
	religion = "border_cults"
	culture = "wexonard"

	trait = race_human

	father = ryonard_0009
	
	954.4.6 = {
		birth = yes
	}
	978.4.27 = {
		death = {
			death_reason = death_butchers_field_massacre
			killer = butcherson_0002	#Butcher's Field Massacre
		}
	}
}

rhinmond_0002 = {
	name = "Teagan"
	#dynasty = house_rhinmond #Sugambic
	dynasty_house = house_rhinmond
	religion = "border_cults"
	culture = "wexonard"

	trait = race_human
	trait = education_learning_4
	trait = trusting
	trait = gregarious
	trait = stubborn

	disallow_random_traits = yes

	father = rhinmond_0001
	
	974.7.26 = {
		birth = yes
	}
	978.4.27 = {	#butchers field massacre
		add_trait = scarred
	}
}

emilard_0001 = {
	name = "Ewald"
	dynasty = dynasty_emilard
	religion = "border_cults"
	culture = "wexonard"

	trait = race_human
	
	989.5.22 = {
		birth = yes
	}
}

necropolis_0001 = { # High Priest Folcard of the Necropolis
	name = "Folcard"
	# lowborn
	culture = oakfoot_halfling
	religion = border_cults
	
	trait = race_halfling
	trait = education_learning_4
	trait = zealous
	trait = compassionate
	trait = humble
	trait = lifestyle_physician
	
	960.11.11 = {
		birth = yes
	}
	
	990.1.1.1 = {
		effect = {
			add_trait_xp = {
				trait = lifestyle_physician
				value = 100
			}
		}
	}
}

necropolis_0002 = { # High Priest Folcard of the Necropolis
	name = "Vincent"
	dynasty = dynasty_jonard
	culture = wexonard
	religion = border_cults
	
	trait = race_human

	979.1.3 = {
		birth = yes
	}
}

