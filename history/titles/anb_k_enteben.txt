
k_enteben = {
	1000.1.1 = { change_development_level = 8 }
	
	550.8.17 = {
		holder = caylentis_0001 # Caylen I Caylentis
	}
	605.4.14 = {
		holder = caylentis_0004 # Trystane I Caylentis
	}
	622.9.15 = {
		holder = caylentis_0005 # Caylen II Caylentis
	}
	640.4.7 = {
		holder = caylentis_0006 # Caylen III Caylentis
	}
	659.11.30 = {
		holder = caylentis_0016 # Aedan I Caylentis
	}
	690.8.12 = {
		holder = caylentis_0017 # Caylen IV Caylentis
	}
	725.4.14 = {
		holder = caylentis_0019 # Aedan II Caylentis
	}
	767.5.3 = {
		holder = caylentis_0021 # Caylen V Caylentis
	}
	783.3.28 = {
		holder = caylentis_0022 # Caylen VI Caylentis
	}
	805.7.26 = {
		holder = caylentis_0023 # Trystane II Caylentis
	}
	818.9.9 = {
		holder = caylentis_0024 # Caylen VII Caylentis
	}
	855.9.12 = {
		holder = caylentis_0025 # Caylen VIII Caylentis
	}
	885.9.30 = {
		holder = caylentis_0027 # Gawan I Caylentis
	}
	895.12.19 = {
		holder = lorentis_0001 # Lorevarn II Lorentis
	}
	900.1.1 = { # War of the One Rose
	 	holder = 0
	}
}


d_great_ording = {
	603.5.30 = {
		holder = caylentis_0008
	}
	624.7.14 = {
		holder = caylentis_0009
	}
	641.5.11 = {
		holder = caylentis_0010
	}
	673.10.10 = {
		holder = 0
	}
	
	900.1.1 = { # War of the One Rose
		liege = k_lorent
	}
	1001.07.03 = {
		holder = 160
	}
}

c_high_ording = {
	1001.07.03 = {
		holder = 160
	}
}

#b_casnaview = { #Commented out for now, todo fix this
#	1001.01.01 = {
#		holder = 20006
#	}
#}

c_east_ording = {
	1001.07.03 = {
		holder = 160
	}
}

c_west_ording = {
	1001.07.03= {
		holder = 160
	}
}

d_horsegarden = {
	1000.1.1 = { change_development_level = 9 }
	900.1.1 = { # War of the One Rose
		liege = k_lorent
	}
}

c_horsegarden = {
	900.1.1 = { # War of the One Rose
		liege = k_lorent
	}
	1015.5.12 = {
		holder = 596	#Lunaor Gwevoris
	}
}

c_greatfield = {
	900.1.1 = { # War of the One Rose
		liege = k_lorent
	}
	992.9.25 = {
		holder = 588	#Gawan Brannis
	}
}

d_crovania = {
	1000.1.1 = { change_development_level = 7 }
	900.1.1 = { # War of the One Rose
		liege = k_lorent
	}
	1022.1.1 = {
		holder = 2	#temporarily until they get actual holders
	}
}

c_mistspear = {
	900.1.1 = { # War of the One Rose
		liege = k_lorent
	}
	986.7.10 = {
		holder = 593	#Griselda Modrentis, Widow of Mistspear
	}
}

c_crovans_rest = {
	900.1.1 = { # War of the One Rose
		liege = k_lorent
	}
	986.7.10 = {
		holder = 592	#Drunhem Modrentis
	}
}

c_new_adea = {
	900.1.1 = { # War of the One Rose
		liege = k_lorent
	}
	989.7.3 = {
		holder = 597	#Belan Calonis
	}
}

d_enteben = {
	900.1.1 = { # War of the One Rose
		liege = k_lorent
	}
	988.9.25 = {
		holder = 582	#Roen the Elder Arthelis
	}
	1014.7.14 = {
		holder = 583	#Roen the Younger Arthelis. His father dies in Battle of Morban Flats
	}
}

c_enteben = {
	900.1.1 = { # War of the One Rose
		liege = k_lorent
	}
	988.9.25 = {
		holder = 582	#Roen the Elder Arthelis
	}
	1014.7.14 = {
		holder = 583	#Roen the Younger Arthelis. His father dies in Battle of Morban Flats
	}
}

c_saddleglade = {
	900.1.1 = { # War of the One Rose
		liege = k_lorent
	}
	988.9.25 = {
		holder = 582	#Roen the Elder Arthelis
	}
	1014.7.14 = {
		holder = 583	#Roen the Younger Arthelis. His father dies in Battle of Morban Flats
	}
}

c_wagonwood = {
	900.1.1 = { # War of the One Rose
		liege = k_lorent
	}
	992.9.25 = {
		holder = 588	#Gawan Brannis
	}
}